import React, {useState} from 'react';
import './style.scss';
import img from './react.png';

export const Root = () => {
    const [clicked, setClicked] = useState(true);

    return (
        <div>
            <button className="root-button" onClick={() => setClicked(!clicked)}>
                {clicked ? 'clicked' : 'not clicked'}
            </button>
            <br />
            <img src={img} alt="img" width="300px"/>
        </div>
    )
}